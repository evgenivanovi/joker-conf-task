public class Movie5 {

    public static void main(String[] args){
        Object pencil = new Object();
        new Trick().process(pencil);
    }

    static class Trick {
        void process(Object object){
            object = null;
            System.gc();
        }
    }

}