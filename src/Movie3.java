import java.time.LocalDateTime;
import java.time.Month;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class Movie3 {

    public static void main(String[] args) {

        ExecutorService story = Executors.newCachedThreadPool();

        Future<?> transportationTaskStatus = story.submit(new ShipThread());

        Object man = new Object();
        Object woman = new Object();

        story.submit(new LoveThread(man, woman));

        try {
            transportationTaskStatus.get();
        } catch (ExecutionException | InterruptedException ex) {
            story.shutdownNow();
        }

    }

    private static class ShipThread extends Thread {

        public void run() {
            while(!LocalDateTime.now().isEqual(LocalDateTime.of(1912, Month.APRIL, 14, 23, 39)));
            throw new CrashException();
        }

    }

    private static class LoveThread extends Thread {

        LoveThread(Object man, Object woman) { }

        public void run() {}

    }

    private static class CrashException extends RuntimeException {}

}
