public class Movie4 {

    public static void main(String[] args) {
        Person oldPerson = new Person();
        Person youngPerson = new Person(oldPerson);

        youngPerson.ask(oldPerson);
    }

    static class Person {

        private Person respectTo;

        Person() {
            this.respectTo = this;
        }

        Person(Person respectTo) {
            this.respectTo = respectTo;
        }

        boolean ask(Person person){
            return person.equals(respectTo);
        }

    }

}