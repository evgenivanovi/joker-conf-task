import java.util.Random;

import static java.lang.System.out;

public class Movie2 {

    static final int SOME_LAST_WORKING_DAY = new Random().nextInt(Integer.MAX_VALUE);

    public static void main(String[] args) {

        for (int day = 1; day <= SOME_LAST_WORKING_DAY; day++) {

            ProfessorThread professorThread = new ProfessorThread(day);
            DomesticThread bestFriendThread = new DomesticThread(professorThread);

            professorThread.start();
            bestFriendThread.start();
        }

    }

    static class ProfessorThread extends Thread {
        int workingDayNumber;

        ProfessorThread(int workingDayNumber) {
            this.workingDayNumber = workingDayNumber;
        }

        public void run() {
            out.println("Work");

            if (workingDayNumber == SOME_LAST_WORKING_DAY) {
                while(true);
            } else {
                out.println("Go Home");
            }
        }

    }

    static class DomesticThread extends Thread {
        Thread professorThread;

        DomesticThread(ProfessorThread professorThread) {
            this.professorThread = professorThread;
        }

        public void run() {
            try {
                professorThread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }

}
