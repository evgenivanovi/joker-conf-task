import java.util.LinkedList;
import java.util.Random;

public class Movie6 {

    public static void main(String[] args){
        Person oscar = new Person();

        for (int i = 0; i < 1200; i++){
            oscar.safePrisoner();
        }
    }

    static class Person {
        private LinkedList<String> list = new LinkedList<>();

        void safePrisoner() {
            list.add(namePrisoner());
        }
    }

    private static String namePrisoner() {
        return Long.toString(Math.abs(new Random().nextLong() % 3656158440062976L), 36);
    }

}