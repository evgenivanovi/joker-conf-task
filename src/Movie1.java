import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Map;
import java.util.Random;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.lang.System.out;

public class Movie1 {

    static Collection<Object> people = new ArrayList<>();
    static Collection<Object> blueCreatures = new ArrayList<>();

    static Object mainHero = new Object();

    public static void main(String[] args) throws Exception {

        Collection<Object> blueCreaturesWithSomeone = Stream.of(mainHero)
                .peek(putInCapsule)
                .map(transformToBlueCreature)
                .collect(Collectors.toCollection(() -> Movie1.blueCreatures));

        blueCreaturesWithSomeone.stream()
                .filter(isMainHero)
                .limit(1)
                .peek(marry)
                .peek(becomeLeader);

        Stream.concat(blueCreaturesWithSomeone.stream(), people.stream())
                .peek(startWar)
                .parallel()
                .filter(isAlive)
                .collect(Collectors.groupingBy(o -> isBlueCreature.test(o), Collectors.counting()))
                .entrySet()
                .stream()
                .max(Comparator.comparing(Map.Entry::getValue))
                .map(Map.Entry::getKey)
                .filter(isBlueCreature)
                .orElseThrow(Exception::new);
    }

    static UnaryOperator<Object> transformToBlueCreature = o -> new Object();

    static Consumer<Object> putInCapsule = o -> out.println("Put in capsule");
    static Consumer<Object> becomeLeader = o -> out.println("Become Leader");
    static Consumer<Object> marry = o -> out.println("Marry");
    static Consumer<Object> startWar = o -> out.println("Start War");

    static Predicate<Object> isAlive = o -> new Random().nextBoolean();
    static Predicate<Object> isBlueCreature = o -> new Random().nextBoolean();
    static Predicate<Object> isMainHero = o -> new Random().nextBoolean();

}
